package contentnegotiation_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	contentnegotiation "gitlab.com/jamietanna/content-negotiation-go"
)

func ExampleParseAcceptHeader_fromBrowser() {
	parsed := contentnegotiation.ParseAcceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8")
	for _, mt := range parsed {
		fmt.Println(mt.String())
	}

	// output:
	// text/html
	// application/xhtml+xml
	// image/avif
	// image/webp
	// application/xml; q=0.9
	// */*; q=0.8
}

func ExampleParseAcceptHeader_sort() {
	parsed := contentnegotiation.ParseAcceptHeader("application/json, application/json;v=1;foo=bar, application/xml;q=0.1")
	for _, mt := range parsed {
		fmt.Println(mt.String())
	}

	// output:
	// application/json; foo=bar; v=1
	// application/json
	// application/xml; q=0.1
}

func ExampleParseAcceptHeaders() {
	parsed := contentnegotiation.ParseAcceptHeaders("text/html", "application/json")
	for _, mt := range parsed {
		fmt.Println(mt.String())
	}

	// output:
	// text/html
	// application/json
}

func TestParseAcceptHeader(t *testing.T) {
	t.Run("when empty", func(t *testing.T) {
		parsed := contentnegotiation.ParseAcceptHeader("")

		assert.Len(t, parsed, 0)
	})

	t.Run("when one value", func(t *testing.T) {
		parsed := contentnegotiation.ParseAcceptHeader("text/plain")

		assert.Len(t, parsed, 1)
		containsMediaType(t, parsed, "text/plain")
	})

	t.Run("when multiple values", func(t *testing.T) {
		parsed := contentnegotiation.ParseAcceptHeader("text/plain; q=0.5, text/html, text/x-dvi; q=0.8, text/x-c")

		assert.Len(t, parsed, 4)
		containsMediaType(t, parsed, "text/plain; q=0.5")
		containsMediaType(t, parsed, "text/html")
		containsMediaType(t, parsed, "text/x-dvi; q=0.8")
		containsMediaType(t, parsed, "text/x-c")
	})

	t.Run("returns in order of quality weighting", func(t *testing.T) {
		parsed := contentnegotiation.ParseAcceptHeader("text/plain; q=0.5, text/html")

		expected := []contentnegotiation.MediaType{*contentnegotiation.NewMediaType("text/html"), *contentnegotiation.NewMediaType("text/plain; q=0.5")}

		assert.Equal(t, expected, parsed)
	})
}

func TestParseAcceptHeaders(t *testing.T) {
	t.Run("when empty", func(t *testing.T) {
		parsed := contentnegotiation.ParseAcceptHeaders()

		assert.Len(t, parsed, 0)
	})

	t.Run("when one value", func(t *testing.T) {
		parsed := contentnegotiation.ParseAcceptHeaders("text/plain")

		assert.Len(t, parsed, 1)
		containsMediaType(t, parsed, "text/plain")
	})

	t.Run("when multiple values", func(t *testing.T) {
		parsed := contentnegotiation.ParseAcceptHeaders("text/plain; q=0.5", "text/html")

		assert.Len(t, parsed, 2)
		containsMediaType(t, parsed, "text/plain; q=0.5")
		containsMediaType(t, parsed, "text/html")
	})

	t.Run("returns in order of quality weighting", func(t *testing.T) {
		parsed := contentnegotiation.ParseAcceptHeaders("text/plain; q=0.5", "text/html")

		expected := []contentnegotiation.MediaType{*contentnegotiation.NewMediaType("text/html"), *contentnegotiation.NewMediaType("text/plain; q=0.5")}

		assert.Equal(t, expected, parsed)
	})
}

func containsMediaType(t *testing.T, parsed []contentnegotiation.MediaType, expected string) {
	assert.Contains(t, parsed, *contentnegotiation.NewMediaType(expected))
}
