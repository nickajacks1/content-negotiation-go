package contentnegotiation

var (
	noMatches       = ErrNotAcceptable{"no matches were found"}
	multipleMatches = ErrNotAcceptable{"multiple matches were found"}
)

// Negotiator is a type that can, given some pre-configured supported `MediaType`s, determine which is the most relevant MediaType to return
type Negotiator struct {
	supported []MediaType
}

func contains(sl []MediaType, expected MediaType) bool {
	for _, value := range sl {
		if value.Equals(&expected) {
			return true
		}
	}
	return false
}

// NewNegotiator create a new Negotiator with a list of supported `MediaType`s
func NewNegotiator(supported ...string) Negotiator {
	var mediaTypes []MediaType
	for _, v := range supported {
		mediaType := NewMediaType(v)
		if mediaType != nil {
			delete(mediaType.params, "q")
			mediaTypes = append(mediaTypes, *mediaType)
		}
	}

	if len(supported) == 0 {
		mediaTypes = append(mediaTypes, WildcardType)
	}

	if contains(mediaTypes, WildcardType) {
		mediaTypes = []MediaType{WildcardType}
	}

	return Negotiator{
		supported: mediaTypes,
	}
}

// ErrNotAcceptable maps to an HTTP 406 Not Acceptable error, or an HTTP 415 Unsupported Media Type if used for `Content-Type` header negotiation
type ErrNotAcceptable struct {
	message string
}

func (e *ErrNotAcceptable) Error() string {
	return e.message
}

// Negotiate perform content-type negotiation against the configured options, returning the server-side supported media type, the client-side requested media type, or an error
func (n Negotiator) Negotiate(acceptHeader string) (MediaType, MediaType, error) {
	requested := ParseAcceptHeader(acceptHeader)
	var ms []match

	for _, mt := range requested {
		for _, supported := range n.supported {
			if WildcardType.Equals(&supported) {
				// we can return early here, because the wildcard should always take priority
				return WildcardType, mt, nil
			}
			if mt.Equals(&supported) {
				ms = append(ms, match{supported, mt})
			} else if mt.IsCompatibleWith(&supported) {
				ms = append(ms, match{supported, mt})
			}
		}
	}

	if len(ms) == 0 {
		return MediaType{}, MediaType{}, &noMatches
	}

	// ensure that the highest resolved + provided MediaType doesn't match multiple cases
	var matchedAndSupported []MediaType
	best := ms[0].provided
	for _, supported := range n.supported {
		if best.IsCompatibleWith(&supported) {
			matchedAndSupported = append(matchedAndSupported, supported)
		}
	}

	if len(matchedAndSupported) == 0 {
		return MediaType{}, MediaType{}, &noMatches
	}

	if len(matchedAndSupported) != 1 {
		return MediaType{}, MediaType{}, &multipleMatches
	}

	return ms[0].supported, ms[0].provided, nil
}

type match struct {
	supported MediaType
	provided  MediaType
}
