package contentnegotiation_test

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	contentnegotiation "gitlab.com/jamietanna/content-negotiation-go"
)

func ExampleNegotiator_Negotiate() {
	negotiator := contentnegotiation.NewNegotiator("application/json")
	negotiated, provided, err := negotiator.Negotiate("application/json")
	if err == nil {
		fmt.Println(negotiated.String())
		fmt.Println(provided.String())
	}
	// output:
	// application/json
	// application/json
}

func ExampleNegotiator_Negotiate_noArgs() {
	negotiator := contentnegotiation.NewNegotiator()
	negotiated, provided, err := negotiator.Negotiate("application/json")
	if err == nil {
		fmt.Println(negotiated.String())
		fmt.Println(provided.String())
	}
	// output:
	// */*
	// application/json
}

func ExampleNegotiator_Negotiate_suffix() {
	negotiator := contentnegotiation.NewNegotiator("application/xml", "application/json")
	negotiated, provided, err := negotiator.Negotiate("application/*+json")
	if err == nil {
		fmt.Println(negotiated.String())
		fmt.Println(provided.String())
	}
	// output:
	// application/json
	// application/*+json
}

func TestNegotiator_Negotiate(t *testing.T) {
	t.Run("when no-args constructor", func(t *testing.T) {
		negotiator := contentnegotiation.NewNegotiator()

		cases := []string{"application/json", "text/plain", "application/*", "*/*"}

		for _, mediaType := range cases {
			t.Run("matches "+mediaType, func(t *testing.T) {
				_, _, err := negotiator.Negotiate(mediaType)

				assert.NoError(t, err)
			})

			t.Run("returns the configured type that was matched, which is the wildcard type", func(t *testing.T) {
				mediaType, _, err := negotiator.Negotiate(mediaType)

				assert.NoError(t, err)
				assert.Equal(t, "*/*", mediaType.String())
			})

			t.Run("returns the provided type", func(t *testing.T) {
				_, provided, err := negotiator.Negotiate(mediaType)

				assert.NoError(t, err)
				assert.Equal(t, mediaType, provided.String())
			})
		}
	})

	t.Run("when wildcard type and concrete media types configured, it returns the wildcard type on matches", func(t *testing.T) {
		negotiator := contentnegotiation.NewNegotiator("*/*", "application/json")
		mediaType, provided, err := negotiator.Negotiate("application/json")

		assert.NoError(t, err)
		assert.Equal(t, "*/*", mediaType.String())
		assert.Equal(t, "application/json", provided.String())
	})

	t.Run("returns ErrNotAcceptable when no matches found", func(t *testing.T) {
		negotiator := contentnegotiation.NewNegotiator("application/json")

		_, _, err := negotiator.Negotiate("text/plain")

		assert.Error(t, err, "no matches were found")
		assert.IsType(t, err, &contentnegotiation.ErrNotAcceptable{})
	})

	t.Run("returns ErrNotAcceptable when multiple matches found", func(t *testing.T) {
		t.Run("when wildcard type", func(t *testing.T) {
			negotiator := contentnegotiation.NewNegotiator("text/plain", "application/vnd.api+json")

			_, _, err := negotiator.Negotiate("*/*")

			assert.Error(t, err, "multiple matches were found")
			assert.IsType(t, err, &contentnegotiation.ErrNotAcceptable{})
		})

		t.Run("when wildcard subtype", func(t *testing.T) {
			negotiator := contentnegotiation.NewNegotiator("application/json", "application/vnd.api+json")

			_, _, err := negotiator.Negotiate("application/*")

			assert.Error(t, err, "multiple matches were found")
			assert.IsType(t, err, &contentnegotiation.ErrNotAcceptable{})
		})

		t.Run("when wildcard subtype with suffix", func(t *testing.T) {
			negotiator := contentnegotiation.NewNegotiator("application/json", "application/vnd.api+json")

			_, _, err := negotiator.Negotiate("application/*+json")

			assert.Error(t, err, "multiple matches were found")
			assert.IsType(t, err, &contentnegotiation.ErrNotAcceptable{})
		})
	})

	t.Run("performs quality-based negotiation", func(t *testing.T) {
		negotiator := contentnegotiation.NewNegotiator("application/json", "application/vnd.api+json")

		t.Run("when no quality is set, q=1 is implied", func(t *testing.T) {
			mediaType, _, err := negotiator.Negotiate("application/json")

			assert.NoError(t, err)
			assert.Equal(t, "application/json", mediaType.String())
		})

		t.Run("when quality is set, with one at q=1, it is abided by", func(t *testing.T) {
			mediaType, _, err := negotiator.Negotiate("application/json; q=0.3, application/vnd.api+json")

			assert.NoError(t, err)
			assert.Equal(t, "application/vnd.api+json", mediaType.String())
		})

		t.Run("when quality is set, with both at fractional values, it is abided by", func(t *testing.T) {
			mediaType, provided, err := negotiator.Negotiate("application/json; q=0.3, application/vnd.api+json;q=0.9")

			assert.NoError(t, err)
			assert.Equal(t, "application/vnd.api+json", mediaType.String())
			assert.Equal(t, "application/vnd.api+json; q=0.9", provided.String())
		})
	})

	t.Run("performs specificity-based negotiation", func(t *testing.T) {
		negotiator := contentnegotiation.NewNegotiator("application/json")

		mediaType, _, err := negotiator.Negotiate("application/*")

		assert.NoError(t, err)
		assert.Equal(t, "application/json", mediaType.String())
	})

	t.Run("ignores other parameters", func(t *testing.T) {
		negotiator := contentnegotiation.NewNegotiator("application/json;v=1")

		mediaType, provided, err := negotiator.Negotiate("application/json")

		assert.NoError(t, err)
		assert.Equal(t, "application/json; v=1", mediaType.String())
		assert.Equal(t, "application/json", provided.String())
	})

	t.Run("quality values are ignored when set on the constructor", func(t *testing.T) {
		negotiator := contentnegotiation.NewNegotiator("text/plain; q=0.5")

		mediaType, _, err := negotiator.Negotiate("text/plain")

		assert.NoError(t, err)
		assert.Equal(t, "text/plain", mediaType.String())
	})

	t.Run("suffixes can be used", func(t *testing.T) {
		negotiator := contentnegotiation.NewNegotiator("application/json")

		mediaType, _, err := negotiator.Negotiate("application/*+json")

		assert.NoError(t, err)
		assert.Equal(t, "application/json", mediaType.String())
	})

	t.Run("RFC7231 example", func(t *testing.T) {
		plain := "text/plain; q=0.5"
		html := "text/html"
		xdvi := "text/x-dvi; q=0.8"
		xc := "text/x-c"

		negotiator := contentnegotiation.NewNegotiator(plain, html, xdvi, xc)

		t.Run("q=1 has an equal chance of negotiation", func(t *testing.T) {
			cases := []string{html, xc}
			for _, v := range cases {
				t.Run("when "+v, func(t *testing.T) {
					mediaType, provided, err := negotiator.Negotiate(v)

					assert.NoError(t, err)
					assert.Equal(t, v, mediaType.String())
					assert.Equal(t, v, provided.String())
				})
			}
		})

		t.Run("q=1 is selected", func(t *testing.T) {
			mediaType, provided, err := negotiator.Negotiate(acceptHeader(plain, html, xdvi, xc))

			expected := []string{html, xc}

			assert.NoError(t, err)
			assert.Contains(t, expected, mediaType.String())
			assert.Contains(t, expected, provided.String())
		})

		t.Run("q=1 is selected is requested alongside lower quality", func(t *testing.T) {
			mediaType, provided, err := negotiator.Negotiate(acceptHeader(html, xdvi))

			assert.NoError(t, err)
			assert.Equal(t, html, mediaType.String())
			assert.Equal(t, html, provided.String())
		})

		t.Run("a lower quality media type can be selected", func(t *testing.T) {
			mediaType, provided, err := negotiator.Negotiate(xdvi)

			assert.NoError(t, err)
			assert.Equal(t, "text/x-dvi", mediaType.String())
			assert.Equal(t, xdvi, provided.String())
		})

		t.Run("the lowest quality media type can be resolved", func(t *testing.T) {
			mediaType, provided, err := negotiator.Negotiate(plain)

			assert.NoError(t, err)
			assert.Equal(t, "text/plain", mediaType.String())
			assert.Equal(t, plain, provided.String())
		})
	})
}

func TestNegotiator_Negotiate_ExternalTestCases(t *testing.T) {
	var file struct {
		Cases []struct {
			Name         string   `json:"name"`
			Supported    []string `json:"supported"`
			AcceptHeader string   `json:"acceptHeader"`
			Negotiated   []string `json:"negotiated"`
		} `json:"cases"`
	}

	bytes, err := os.ReadFile("./testdata/content-negotiation-test-cases/cases.json")
	if err != nil {
		t.Errorf("Failed to open cases.json from Git Submodule")
	}
	json.Unmarshal(bytes, &file)

	for _, v := range file.Cases {
		t.Run(v.Name, func(t *testing.T) {
			negotiator := contentnegotiation.NewNegotiator(v.Supported...)

			mediaType, _, err := negotiator.Negotiate(v.AcceptHeader)

			if len(v.Negotiated) == 0 {
				assert.Error(t, err)
			} else if len(v.Negotiated) == 1 {
				assert.Equal(t, v.Negotiated[0], mediaType.String())
			} else {
				assert.Contains(t, v.Negotiated, mediaType.String())
			}
		})
	}
}

func acceptHeader(values ...string) string {
	return strings.Join(values, ", ")
}
