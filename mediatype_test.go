package contentnegotiation_test

import (
	"fmt"
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
	contentnegotiation "gitlab.com/jamietanna/content-negotiation-go"
)

func TestNewMediaType(t *testing.T) {
	validCases := []string{"application/json", "text/plain", "application/*", "*/*", "application/json-patch+json", "application/json;v=1", "application/json ; v=1", "application/vnd.me.jvt.api.v1+json", "application/*+json", "multipart/form-data"}

	for _, v := range validCases {
		t.Run(fmt.Sprintf("when valid (%s), does not return nil", v), func(t *testing.T) {
			mediaType := contentnegotiation.NewMediaType(v)
			assert.NotNil(t, mediaType)
		})
	}

	invalidCases := []string{"", "application/", "application", "*/json", "audio/*; q=0.2, audio/basic"}

	for _, v := range invalidCases {
		t.Run(fmt.Sprintf("when invalid (%s), returns nil", v), func(t *testing.T) {
			mediaType := contentnegotiation.NewMediaType(v)
			assert.Nil(t, mediaType)
		})
	}
}

var testCases = []struct {
	name                  string
	mediaType             string
	expectedType          string
	expectedSubType       string
	expectedSubTypeSuffix string
}{
	{
		name:            "when application/json",
		mediaType:       "application/json",
		expectedType:    "application",
		expectedSubType: "json",
	},
	{
		name:            "when text/plain",
		mediaType:       "text/plain",
		expectedType:    "text",
		expectedSubType: "plain",
	},
	{
		name:            "when wildcard type",
		mediaType:       "*/*",
		expectedType:    "*",
		expectedSubType: "*",
	},
	{
		name:            "when wildcard subtype",
		mediaType:       "application/*",
		expectedType:    "application",
		expectedSubType: "*",
	},
	{
		name:                  "when wildcard subtype with suffix",
		mediaType:             "application/*+json",
		expectedType:          "application",
		expectedSubType:       "*+json",
		expectedSubTypeSuffix: "json",
	},
	{
		name:                  "when suffix",
		mediaType:             "application/vnd.api+json",
		expectedType:          "application",
		expectedSubType:       "vnd.api+json",
		expectedSubTypeSuffix: "json",
	},
}

func TestMediaType_GetType(t *testing.T) {
	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			mediaType := contentnegotiation.NewMediaType(tt.mediaType)

			assert.Equal(t, tt.expectedType, mediaType.GetType())
		})
	}
}

func TestMediaType_GetSubType(t *testing.T) {
	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			mediaType := contentnegotiation.NewMediaType(tt.mediaType)

			assert.Equal(t, tt.expectedSubType, mediaType.GetSubType())
		})
	}
}

func TestMediaType_GetSubTypeSuffix(t *testing.T) {
	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			mediaType := contentnegotiation.NewMediaType(tt.mediaType)

			assert.Equal(t, tt.expectedSubTypeSuffix, mediaType.GetSubTypeSuffix())
		})
	}
}

func BenchmarkMediaType_GetSubTypeSuffix(b *testing.B) {
	for _, tt := range testCases {
		b.Run(tt.name, func(b *testing.B) {
			var mediaType *contentnegotiation.MediaType

			for n := 0; n < b.N; n++ {
				mediaType = contentnegotiation.NewMediaType(tt.mediaType)
			}

			assert.Equal(b, tt.expectedSubType, mediaType.GetSubType())
		})
	}
}

func TestMediaType_IsWildcardType(t *testing.T) {
	cases := []struct {
		name      string
		mediaType string
		expected  bool
	}{
		{
			name:      "when application/json",
			mediaType: "application/json",
		},
		{
			name:      "when text/plain",
			mediaType: "text/plain",
		},
		{
			name:      "when wildcard type",
			mediaType: "*/*",
			expected:  true,
		},
		{
			name:      "when wildcard subtype",
			mediaType: "application/*",
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			mediaType := contentnegotiation.NewMediaType(tt.mediaType)

			assert.Equal(t, tt.expected, mediaType.IsWildcardType())
		})
	}
}

func TestMediaType_IsWildcardSubType(t *testing.T) {
	cases := []struct {
		name      string
		mediaType string
		expected  bool
	}{
		{
			name:      "when application/json",
			mediaType: "application/json",
		},
		{
			name:      "when text/plain",
			mediaType: "text/plain",
		},
		{
			name:      "when wildcard type",
			mediaType: "*/*",
			expected:  true,
		},
		{
			name:      "when wildcard subtype",
			mediaType: "application/*",
			expected:  true,
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			mediaType := contentnegotiation.NewMediaType(tt.mediaType)

			assert.Equal(t, tt.expected, mediaType.IsWildcardSubType())
		})
	}
}

func TestMediaType_GetParameters(t *testing.T) {
	t.Run("when none provided, returns empty map", func(t *testing.T) {
		mediaType := contentnegotiation.NewMediaType("application/json")

		assert.Empty(t, mediaType.GetParameters())
	})

	t.Run("when q=1, returns empty map", func(t *testing.T) {
		mediaType := contentnegotiation.NewMediaType("application/json;q=1")

		assert.Empty(t, mediaType.GetParameters())
	})

	t.Run("when q=1.0, returns empty map", func(t *testing.T) {
		mediaType := contentnegotiation.NewMediaType("application/json;q=1.0")

		assert.Empty(t, mediaType.GetParameters())
	})

	t.Run("when one parameter", func(t *testing.T) {
		mediaType := contentnegotiation.NewMediaType("application/json;key=value")

		assertMapValue(t, mediaType.GetParameters(), "key", "value")
	})

	t.Run("when many parameters", func(t *testing.T) {
		mediaType := contentnegotiation.NewMediaType("application/json;v=1;q=0;charset=abcdef")

		params := mediaType.GetParameters()
		assertMapValue(t, params, "v", "1")
		assertMapValue(t, params, "q", "0")
		assertMapValue(t, params, "charset", "abcdef")
	})

	t.Run("parameter key is lowercased, but value remains the same", func(t *testing.T) {
		mediaType := contentnegotiation.NewMediaType("application/json;KEY=vAlUe")

		params := mediaType.GetParameters()
		assertMapValue(t, params, "key", "vAlUe")
	})
}

func TestMediaType_GetQualityValue(t *testing.T) {
	t.Run("when not provided, defaults to 1.0", func(t *testing.T) {
		mediaType := contentnegotiation.NewMediaType("text/plain")

		assert.Equal(t, 1.0, mediaType.GetQualityValue())
	})

	cases := []struct {
		mediaType string
		expected  float64
	}{
		{mediaType: "application/json;q=0.1", expected: 0.1},
		{mediaType: "application/json;q=0.10", expected: 0.1},
		{mediaType: "application/json;q=0.100", expected: 0.1},
		{mediaType: "application/json;q=0", expected: 0},
		{mediaType: "application/json;q=1", expected: 1},
		{mediaType: "application/json ; q=1", expected: 1},
		{mediaType: "application/json;q=1.0", expected: 1},
		{mediaType: "application/json;q=0.123", expected: 0.123},
	}

	for _, tt := range cases {
		t.Run(fmt.Sprintf("when %s", tt.mediaType), func(t *testing.T) {
			mediaType := contentnegotiation.NewMediaType(tt.mediaType)

			assert.Equal(t, threeDecimalPlaces(tt.expected), mediaType.GetQualityValue())
		})
	}

	t.Run("when quality value below 0, defaults to 1.0", func(t *testing.T) {
		mediaType := contentnegotiation.NewMediaType("text/plain;q=-1")

		assert.Equal(t, 1.0, mediaType.GetQualityValue())
	})

	t.Run("when quality value above 1, defaults to 1.0", func(t *testing.T) {
		mediaType := contentnegotiation.NewMediaType("text/plain;q=2")

		assert.Equal(t, 1.0, mediaType.GetQualityValue())
	})

	t.Run("when non-numeric quality value, defaults to 1.0", func(t *testing.T) {
		mediaType := contentnegotiation.NewMediaType("text/plain;q=foo")

		assert.Equal(t, 1.0, mediaType.GetQualityValue())
	})

	t.Run("when more than three decimal places, the quality is trimmed + floored", func(t *testing.T) {
		mediaType := contentnegotiation.NewMediaType("text/plain;q=0.123555555")

		assert.Equal(t, threeDecimalPlaces(0.123), mediaType.GetQualityValue())
	})
}

func TestMediaType_IsCompatibleWith(t *testing.T) {
	t.Run("wildcard type is equal to itself", func(t *testing.T) {
		lhs := contentnegotiation.NewMediaType("*/*")
		rhs := contentnegotiation.NewMediaType("*/*")

		assert.True(t, lhs.IsCompatibleWith(rhs))
		assert.True(t, rhs.IsCompatibleWith(lhs))
	})

	t.Run("wildcard subtype is equal to itself", func(t *testing.T) {
		lhs := contentnegotiation.NewMediaType("text/*")
		rhs := contentnegotiation.NewMediaType("text/*")

		assert.True(t, lhs.IsCompatibleWith(rhs))
		assert.True(t, rhs.IsCompatibleWith(lhs))
	})

	t.Run("does not match when same type but different subtypes", func(t *testing.T) {
		lhs := contentnegotiation.NewMediaType("application/basic")
		rhs := contentnegotiation.NewMediaType("audio/basic")

		assert.False(t, lhs.IsCompatibleWith(rhs))
	})

	t.Run("nil lhs always returns false", func(t *testing.T) {
		var lhs *contentnegotiation.MediaType
		rhs := contentnegotiation.NewMediaType("application/json")

		assert.False(t, lhs.IsCompatibleWith(rhs))
	})

	t.Run("nil rhs always returns false", func(t *testing.T) {
		lhs := contentnegotiation.NewMediaType("application/json")
		var rhs *contentnegotiation.MediaType

		assert.False(t, lhs.IsCompatibleWith(rhs))
	})

	t.Run("different subtype suffix always returns false", func(t *testing.T) {
		lhs := contentnegotiation.NewMediaType("application/foo+json")
		rhs := contentnegotiation.NewMediaType("application/foo+xml")

		assert.False(t, lhs.IsCompatibleWith(rhs))
	})

	t.Run("suffixes only result in equality if they are with a wildcard", func(t *testing.T) {
		lhs := contentnegotiation.NewMediaType("application/vnd.api+json")
		rhs := contentnegotiation.NewMediaType("application/json")

		assert.False(t, lhs.IsCompatibleWith(rhs))
	})

	t.Run("suffixes only result in equality if they are with a wildcard where the suffix matches the type", func(t *testing.T) {
		lhs := contentnegotiation.NewMediaType("application/*+json")
		rhs := contentnegotiation.NewMediaType("application/xml")

		assert.False(t, lhs.IsCompatibleWith(rhs))
	})

	cases := []struct {
		name string
		lhs  string
		rhs  string
	}{
		{
			name: "wildcard type supports concrete media type, when left-hand-side",
			lhs:  "*/*",
			rhs:  "audio/basic",
		},
		{
			name: "wildcard type supports concrete media type, when right-hand-side",
			lhs:  "audio/basic",
			rhs:  "*/*",
		},
		{
			name: "wildcard type supports concrete type, when left-hand-side",
			lhs:  "*/*",
			rhs:  "audio/*",
		},
		{
			name: "wildcard type supports concrete type, when right-hand-side",
			lhs:  "audio/*",
			rhs:  "*/*",
		},
		{
			name: "wildcard subtype supports concrete media type, when left-hand-side",
			lhs:  "audio/*",
			rhs:  "audio/basic",
		},
		{
			name: "wildcard subtype supports concrete media type, when right-hand-side",
			lhs:  "audio/basic",
			rhs:  "audio/*",
		},
		{
			name: "parameters are ignored",
			lhs:  "audio/basic;v=1",
			rhs:  "audio/basic",
		},
		{
			name: "type is case-insensitive",
			lhs:  "APPLICATION/json",
			rhs:  "application/json",
		},
		{
			name: "subtype is case-insensitive",
			lhs:  "application/JsoN",
			rhs:  "application/json",
		},
		{
			name: "suffix on the lhs is equal to wildcard type on the rhs",
			lhs:  "application/vnd.foo+json",
			rhs:  "*/*",
		},
		{
			name: "suffix on the rhs is equal to wildcard type on the lhs",
			lhs:  "*/*",
			rhs:  "application/vnd.foo+json",
		},
		{
			name: "suffix on the lhs is equal to wildcard subtype on the rhs",
			lhs:  "application/vnd.foo+json",
			rhs:  "application/*",
		},
		{
			name: "suffix on the rhs is equal to wildcard subtype on the lhs",
			lhs:  "application/*",
			rhs:  "application/vnd.foo+json",
		},
		{
			name: "",
			lhs:  "application/json",
			rhs:  "application/*+json",
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			lhs := contentnegotiation.NewMediaType(tt.lhs)
			rhs := contentnegotiation.NewMediaType(tt.rhs)

			assert.True(t, lhs.IsCompatibleWith(rhs))
		})
	}
}

func TestMediaType_Equals(t *testing.T) {
	cases := []struct {
		name     string
		lhs      string
		rhs      string
		expected bool
	}{
		{
			name:     "are equal when same type subtype and no params",
			lhs:      "application/foo",
			rhs:      "application/foo",
			expected: true,
		},
		{
			name:     "are equal when same type subtype and params",
			lhs:      "application/foo;foo=bar",
			rhs:      "application/foo;foo=bar",
			expected: true,
		},
		{
			name:     "are not equal when not same type on lhs",
			lhs:      "applications/foo",
			rhs:      "application/foo",
			expected: false,
		},
		{
			name:     "are not equal when not same type on rhs",
			lhs:      "application/foo",
			rhs:      "applications/foo",
			expected: false,
		},
		{
			name:     "are not equal when not same subtype on lhs",
			lhs:      "application/fool",
			rhs:      "application/foo",
			expected: false,
		},
		{
			name:     "are not equal when not same subtype on rhs",
			lhs:      "application/foo",
			rhs:      "application/food",
			expected: false,
		},
		{
			name:     "are not equal when not different parameters",
			lhs:      "application/foo;if=then",
			rhs:      "application/foo;bar=baz",
			expected: false,
		},
	}
	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			lhs := contentnegotiation.NewMediaType(tt.lhs)
			rhs := contentnegotiation.NewMediaType(tt.rhs)

			assert.Equal(t, tt.expected, lhs.Equals(rhs))
		})
	}

	t.Run("nil lhs always returns false", func(t *testing.T) {
		var lhs *contentnegotiation.MediaType
		rhs := contentnegotiation.NewMediaType("application/json")

		assert.False(t, lhs.Equals(rhs))
	})

	t.Run("nil rhs always returns false", func(t *testing.T) {
		lhs := contentnegotiation.NewMediaType("application/json")
		var rhs *contentnegotiation.MediaType

		assert.False(t, lhs.Equals(rhs))
	})

}

func TestMediaType_String(t *testing.T) {
	cases := []struct {
		mediaType string
		expected  string
	}{
		{
			mediaType: "*/*",
			expected:  "*/*",
		},
		{
			mediaType: "application/*",
			expected:  "application/*",
		},
		{
			mediaType: "application/foo;q=0",
			expected:  "application/foo; q=0",
		},
		{
			mediaType: "application/foo;level=1;abc=def;q=0",
			expected:  "application/foo; abc=def; level=1; q=0",
		},
	}

	for _, tt := range cases {
		t.Run(fmt.Sprintf("when %s", tt.mediaType), func(t *testing.T) {
			mediaType := contentnegotiation.NewMediaType(tt.mediaType)

			assert.Equal(t, tt.expected, mediaType.String())
		})
	}
}

func assertMapValue(t *testing.T, m map[string]string, key string, expected string) {
	actual, ok := m[key]
	assert.Truef(t, ok, "expected map to contain key `%s`, but it did not", key)
	assert.Equal(t, expected, actual)
}

// adapted from https://gosamples.dev/round-float/
func threeDecimalPlaces(val float64) float64 {
	ratio := math.Pow(10, float64(3))
	return math.Floor(val*ratio) / ratio
}
