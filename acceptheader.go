package contentnegotiation

import (
	"sort"
	"strings"
)

// ParseAcceptHeader parses a given `accept` HTTP Header to the corresponding `MediaType`s and returns an empty slice if no valid media types could be parsed. The returned slice is sorted by quality values, and specificity (the higher number parameters)
func ParseAcceptHeader(accept string) []MediaType {
	return ParseAcceptHeaders(accept)
}

// ParseAcceptHeaders parses a number of `accept` HTTP Headers to the corresponding `MediaType`s and returns an empty slice if no valid media types could be parsed. The returned slice is sorted by quality values, and specificity (the higher number parameters)
func ParseAcceptHeaders(accepts ...string) (mediaTypes []MediaType) {
	for _, accept := range accepts {
		parts := strings.Split(accept, ",")
		for _, part := range parts {
			mediaType := NewMediaType(part)
			if mediaType != nil {
				mediaTypes = append(mediaTypes, *mediaType)
			}
		}
	}

	sort.SliceStable(mediaTypes, func(i, j int) bool {
		if mediaTypes[i].GetQualityValue() != mediaTypes[j].GetQualityValue() {
			return mediaTypes[i].GetQualityValue() > mediaTypes[j].GetQualityValue()
		}
		return len(mediaTypes[i].GetParameters()) > len(mediaTypes[j].GetParameters())
	})

	return
}
